import React, { Component } from "react";
import { View, StatusBar, Image, Alert, Linking } from "react-native";
import { AppLoading, Asset } from "expo";
import { Button, Card } from "react-native-elements";

//redux connect
import { connect } from "react-redux";
import { removeUserToken } from "../../actions";

import styles from "../../styles";

class Home extends Component {
  state = {
    isLoadingComplete: false
  };

  _loadResourcesAsync = async () => {
    return Promise.all(Asset.loadAsync(require("../../../assets/icon.png")));
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };

  render() {
    const { navigate } = this.props.navigation;

    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <View style={styles.container}>
          <Card containerStyle={styles.homeCard}>
            <Image
              style={styles.homelogo}
              source={require("../../../assets/icon.png")}
            />
            <Button
              icon={{ name: "wallet-membership", type: "material-community", color: "white", marginRight: 5, size: 20 }}
              title="Devenir membre"
              borderRadius={3}

              containerStyle={styles.loginInput}
              onPress={() => {
                Alert.alert(
                  "Devenir membre",
                  "Vous allez être redirigé vers un formulaire à compléter via le site du B4C.",
                  [
                    {
                      text: "OK, super. Allons-y !",
                      onPress: () =>
                        Linking.openURL("https://b4c.be/devenir-membre/")
                    },
                    { text: "Pas maintenant, merci." }
                  ],
                  { cancelable: true }
                );
              }}
            />
            <Button
              icon={{ name: "login", type: "material-community", color: "white", marginRight: 5, size: 20 }}
              title="S'identifier"
              borderRadius={3}
              buttonStyle={{backgroundColor: "#26a85c"}}
              containerStyle={styles.loginInput}
              onPress={() => navigate("SignIn")}
            />
          </Card>
        </View>
      );
    }
  }

  _signOutAsync = () => {
    this.props
      .removeUserToken()
      .then(() => {
        this.props.navigation.navigate("Profile");
      })
      .catch(error => {
        this.setState({ error });
      });
  };
}

const mapStateToProps = state => ({
  token: state.token
});

const mapDispatchToProps = dispatch => ({
  removeUserToken: () => dispatch(removeUserToken())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
