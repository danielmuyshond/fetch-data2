import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  RefreshControl,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import { Button, Card } from 'react-native-elements';
import { withNavigation } from 'react-navigation';
import { getUserToken } from '../../actions';
import sFlatlistItem from './Activites-styles';
import styles from './styles';

const mapStateToProps = state => ({
  token: state.token
});

const mapDispatchToProps = dispatch => ({
  getUserToken: () => dispatch(getUserToken()),
});

// Component Item de la Flatlist. Chaque element de la liste sera rendu suivant ce component
class ActItm extends React.PureComponent {
  // function pour convertir le nombre du mois en mot ex: 01 -> janvier
  _monthNumericToString = monthNum => {
    switch (monthNum) {
      case '01':
        return ' janvier';
      case '02':
        return ' février';
      case '03':
        return ' mars';
      case '04':
        return ' avril';
      case '05':
        return ' mai';
      case '06':
        return ' juin';
      case '07':
        return ' juillet';
      case '08':
        return ' août';
      case '09':
        return ' septembre';
      case '10':
        return ' octobre';
      case '11':
        return ' novembre';
      case '12':
        return ' décembre';
    }
  };

  // function qui retourne les minutes si il y en a. Pour ne pas afficher 12H00 mais 12H et pouvoir quand même afficher 12H45 si il y a un "45"
  _isThereMinutes = minute => {
    if (minute === '00') {
      return '';
    } else {
      return minute;
    }
  };

  // image de l'activite : si Photo_Chemin vide alors un lien vers image standard
  _isThereAnImage = imagePath => {
    if (imagePath === '') {
      return 'https://www.b4c.be/b4c_app_API/placeholder300200.png';
    } else {
      return imagePath;
    }
  };

  // _canAccessDetailsCheck = () => {
  //   this.props
  //     .getUserToken()
  //     .then(() => {
  //       this.props.token.token !== null
  //         ? this.props.displayActDetail(
  //             act.IDACT,
  //             act.Lib,
  //             this._isThereAnImage(act.Photo_Chemin),
  //             act.Date.substr(8, 2) +
  //               this._monthNumericToString(act.Date.substr(5, 2)),
  //             act.Date.substr(11, 2) +
  //               'h' +
  //               this._isThereMinutes(act.Date.substr(14, 2))
  //           )
  //         : this.state.navigation.navigate('Home');
  //     })
  //     .catch(error => {
  //       this.setState({ error });
  //     });
  // };

  render() {
    //ES6
    // ca equivaut a
    // const item = this.props.item
    // const displayActDetail = this.props.displayActDetail

    const { act, displayActDetail } = this.props;

    return (
      <Card
        containerStyle={sFlatlistItem.listItemContainer}
        image={{ uri: this._isThereAnImage(act.Photo_Chemin) }}
        imageStyle={{}}
      >
        <View style={sFlatlistItem.itemTitleContainer}>
          <Text style={sFlatlistItem.itemDate}>
            {act.Date.substr(8, 2)}
            {this._monthNumericToString(act.Date.substr(5, 2))}
          </Text>
          <Text style={sFlatlistItem.itemTitle}>{act.Lib}</Text>
        </View>
        <Button
          titleStyle={{ fontSize: 14 }}
          buttonStyle={{ width: 150 }}
          containerStyle={{ width: 150, alignSelf: 'center', marginTop: 5 }}
          icon={{ name: 'info', type: 'Ionicons', color: 'white', size: 20 }}
          title="Plus d'info"
          backgroundColor="#998702"
          borderRadius={3}
          onPress={() =>  this.props.displayActDetail(
              act.IDACT,
              act.Lib,
              this._isThereAnImage(act.Photo_Chemin),
              act.Date.substr(8, 2) +
                this._monthNumericToString(act.Date.substr(5, 2)),
              act.Date.substr(11, 2) +
                'h' +
                this._isThereMinutes(act.Date.substr(14, 2)))
          }
        />
      </Card>
    );
  }
}

// Flatlist

class ActList extends Component {
  state = {
    fetchActType: 'all',
    arrayholder: null,
    actFromApi: null,
    newActFromApi: null,
    isLoading: true,
    refreshing: false,
    switch1Value: true,
    switch2Value: true,
    selectedIndex: 0
  };

  // Fetch des data sur le SQL Server B4C via l'API codée en PHP, hébergée sur le FTP du site B4C
  async apiActFetch() {
    try {
      let response = await fetch(
        'https://b4c.be/b4c_app_API/get_news.php?acttype=' +
          this.state.fetchActType
      );
      let responseJson = await response.json();
      this.setState({
        actFromApi: responseJson.activites,
        arrayholder: responseJson.activites,
        isLoading: false
      });
    } catch (error) {
      console.error(error);
    }
  }

  componentDidMount() {
    this.apiActFetch();
  }

  // Function navigation à l'écran détail de l'activité dans laquelle on passe les informations
  _displayActDetail = (idAct, actTitle, actPic, actDate, actHeure) => {
    // console.log("Display activite with id " + idAct);
    this.props.navigation.navigate('ActDetail', {
      idAct: idAct,
      actTitle: actTitle,
      actPic: actPic,
      actDate: actDate,
      actHeure: actHeure
    });
  };

  // function onchange du Picker
  updFetchActType = itemValue => {
    this.setState({ fetchActType: itemValue });
  };

  // Refresh affiche l'activity indicator et refetch les data
  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.apiActFetch().then(() => {
      this.setState({ refreshing: false });
    });
  };

  //Filtrer les data pour sortir juste les activités avec l'attribut sélectionné. Ensuite on le remet dans le statece qui va faire rafraichir la FlatList
  filterData = itemValue => {
    const { arrayholder } = this.state; //arrayholder est une copie des données fetchée via l'api
    // var dataholder = arrayholder;
    if (itemValue === 'all') {
      // Si le picker "toutes les activités" est sélectionné alors on filtre le tableau via propriété objet IdLibCatAct
      var filteredData = arrayholder.filter(
        el => el.IdLibCatAct === '1' || el.IdLibCatAct === '2'
      );
    } else {
      var filteredData = arrayholder.filter(el => el.IdLibCatAct === itemValue);
    }
    this.setState({ actFromApi: filteredData });
  }; // filtrer le tableau d'objets activités pour en sortir les activités spécifiques (culture ou club) sans devoir refaire une requête à l'API

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.apiActFetch().then(() => {
      this.setState({ refreshing: false });
    });
  };

  upperCase = t => {
    var text = t;
    var uppercasetext = text.toUpperCase(); //To convert Upper Case
    return uppercasetext;
  };

  updateIndex = selectedIndex => {
    this.setState({ selectedIndex });
  };

  render() {
    // console.log(this.props);
    // console.log(this.props.navigation);

    const { isLoading } = this.state;
    // const buttons = [{ element: bouton1 }, { element: bouton2 }];
    const { selectedIndex } = this.state;
    // header ButtonGroup components

    if (!isLoading) {
      return (
        <View style={styles.container}>
          <View
            style={[
              {
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                position: 'absolute',
                paddingTop: 5,
                top: 0,
                zIndex: 1,
                backgroundColor: '#fffcf4'
              },
              styles.fullDeviceWidth
            ]}
          >
            <Button
              titleStyle={{ fontSize: 16 }}
              icon={{
                name: 'sort',
                type: 'MaterialIcons',
                color: 'white',
                size: 20
              }}
              title="Agenda activités"
              borderRadius={3}
              containerStyle={{
                marginHorizontal: 10,
                marginBottom: 2,
                marginTop: 2
              }}
              buttonStyle={{
                marginHorizontal: 10,
                marginBottom: 2,
                marginTop: 2
              }}
              onPress={() => this.filterData('1')}
            />
            <Button
              titleStyle={{ fontSize: 16 }}
              icon={{
                name: 'sort',
                type: 'MaterialIcons',
                color: 'white',
                size: 20
              }}
              title="Agenda culturel"
              borderRadius={3}
              buttonStyle={{
                marginHorizontal: 10,
                marginBottom: 2,
                marginTop: 2
              }}
              containerStyle={{
                marginHorizontal: 10,
                marginBottom: 2,
                marginTop: 2
              }}
              onPress={() => this.filterData('2')}
            />
            {/* <ButtonGroup
              onPress={this.updateIndex}
              selectedIndex={selectedIndex}
              selectedButtonStyle={{
                backgroundColor: '#ddd7b3'
              }}
              selectedTextStyle={{ color: 'white' }}
              textStyle={{ color: 'white' }}
              buttonStyle={{ backgroundColor: '#998701' }}
              // buttons={[
              //   <Button
              //     icon={{ name: 'sort', type: 'MaterialIcons', color: "white", size: 20 }}
              //     title="Activités club"
              //     borderRadius={3}
              //     containerStyle={{
              //       backgroundColor: '#838f8e',
              //       borderRadius: 3,
              //       marginTop: 10,
              //       marginBottom: 8,
              //       width: 150
              //     }}
              //     onPress={() => this.filterData('1')}
              //   />,
              //   <Button
              //     icon={{ name: 'sort', type: 'MaterialIcons, color: "white", size: 20 }}
              //     title="Culture"
              //     borderRadius={3}
              //     containerStyle={{
              //       backgroundColor: '#838f8e',
              //       borderRadius: 3,
              //       marginTop: 10,
              //       marginBottom: 8,
              //       width: 150
              //     }}
                  onPress={() => this.filterData('2')}
                /> */}
            {/* ]}
              containerStyle={[{ height: 50 }, styles.fullDeviceWidth]}
            /> */}
          </View>
          <FlatList
            container
            containerViewStyle={{ marginTop: 100 }}
            showsVerticalScrollIndicator={false}
            data={this.state.actFromApi}
            renderItem={({ item }) => (
              <ActItm act={item} displayActDetail={this._displayActDetail} />
            )}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
            ListHeaderComponent={() => <View style={{ height: 50 }} />}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#9A8404" />
        </View>
      );
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigation(ActList));

