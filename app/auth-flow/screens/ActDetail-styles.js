import React, { StyleSheet, Platform, Dimensions } from "react-native";

const sActDetails = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "space-around",
    color: "#1B202B",
    fontSize: 12,
    flexDirection: "column",
    paddingHorizontal: 20
  },
  headerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    textAlign: "center",
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 6,
    marginBottom: 0,
    color: "#42413e"
  },
  date: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 0,
    color: "#42413e"
  },
  nomLieu: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 2,
    color: "#42413e"
  },
  heure: {
    fontSize: 22,
    fontWeight: "bold",
    marginBottom: 5,
    color: "#42413e"
  },
  coverpic: {
    marginTop: 18,
    marginLeft: 5,
    backgroundColor: "transparent",
    width: 170,
    height: 170,
    borderRadius: 170/2
  },
  webview: {
    flexDirection: "column",
    width: Dimensions.get('window').width - 20
  }
});

export default sActDetails;
