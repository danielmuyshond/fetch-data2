import React, { Component } from 'react';
import {
  View,
  Image,
  ActivityIndicator,
  ScrollView,
  Dimensions,
  Alert,
  Linking
} from 'react-native';

import { Text, Button, Card } from 'react-native-elements';
import { connect } from 'react-redux';
import { getUserToken } from '../../actions';
import { WebView } from 'react-native'; // pour afficher du html
import openMap from 'react-native-open-maps'; // pour ouvrir maps
import sActDetails from './ActDetail-styles';

const mapStateToProps = state => ({
  token: state.token
});

const mapDispatchToProps = dispatch => ({
  getUserToken: () => dispatch(getUserToken())
});

const script = `
  <script>
    window.location.hash = 1;
      var calculator = document.createElement("div");
      calculator.id = "height-calculator";
      while (document.body.firstChild) {
          calculator.appendChild(document.body.firstChild);
      }
    document.body.appendChild(calculator);
      document.title = calculator.scrollHeight;

      //supprimer les liens et redim images
    window.onload = function() {
      var anchors = document.getElementsByTagName("a");

      for (var i = 0; i < anchors.length; i++) {
       anchors[i].href = "#";
       anchors[i].style.textDecoration = "none";
       anchors[i].style.color = "#998702";
       
      }

      var images = document.getElementsByTagName("img");

      for (var i = 0; i < images.length; i++) {
       images[i].classList.add("maxwidth");
      }
    }
  </script>
  `;
const style = `
  <style>
  body, html, #height-calculator {
      margin: 0;
      padding: 0;
      font-family: Arial, Helvetica, sans-serif;
  }
  #height-calculator {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
  }
  a, a:visited {
    color: #606032;
  }
  .maxwidth {
    max-width: 100%;
    height: auto;
  }
  </style>
  `;

class ActDetail extends React.PureComponent {
  state = {
    isLoading: true,
    data: [],
    Height: 0,
    // isLogged: false
  };

  // Fetch the token from storage then navigate to our appropriate place
  // _bootstrapAsync = () => {
  //   this.props
  //     .getUserToken()
  //     .then(() => {
  //       this.props.token.token !== null
  //         ? this.setState({ isLogged: true })
  //         : this.setState({ isLogged: false });
  //     })
  //     .catch(error => {
  //       this.setState({ error });
  //     });
  // };

  componentDidMount() {
    // this._bootstrapAsync();
    this.apiActFetch();
  }

  static navigationOptions = {
    title: "Détails de l'activité"
  };

  // Fetch des data sur le SQL Server B4C via l'API codée en PHP, hébergée sur le FTP du site B4C
  async apiActFetch() {
    try {
      let response = await fetch(
        'https://b4c.be/b4c_app_API/get_news.php?actid=' +
          this.props.navigation.state.params.idAct
      );
      let responseJson = await response.json();
      this.setState({
        data: responseJson,
        isLoading: false
      });
      // console.log(this.state.data);
    } catch (error) {
      console.error(error);
    }
  }

  onNavigationChange(event) {
    if (event.title) {
      const htmlHeight = Number(event.title); //convert to number
      this.setState({ Height: htmlHeight });
    }
  }

  render() {
    const { isLoading } = this.state;
    const descriptionWebview = `
      <html>
        <head>
          <meta charset="UTF-8">
          <meta http-equiv="content-type" content="text/html; charset=utf-8">
          <meta name="viewport" content="width=${Dimensions.get('window')
            .width -
            40}, content="initial-scale=0.5, maximum-scale=0.5, user-scalable=no">
        </head>
        <body>
        ${this.state.data.DescriptionWeb}
        </body>
      </html>
    `;
    if ( this.props.token.token !== null) {
      if (!isLoading) {
        return (
          <ScrollView contentContainerStyle={sActDetails.container}>
            <View style={sActDetails.headerContainer}>
              <Image
                style={sActDetails.coverpic}
                source={{ uri: this.props.navigation.state.params.actPic }}
              />
              <Text style={sActDetails.title}>
                {this.props.navigation.state.params.actTitle}
              </Text>
              <Text style={sActDetails.date}>
                {this.props.navigation.state.params.asctDate}
              </Text>
              <Text style={sActDetails.nomLieu}>
                {this.state.data.Nom_Lieu}
              </Text>
              <Text style={sActDetails.heure}>
                {this.props.navigation.state.params.actHeure}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={{
                    fontSize: 13,
                    alignItems: 'center',
                    textAlign: 'center',
                    marginBottom: 2
                  }}
                >
                  {this.state.data.Rue +
                    '\r' +
                    this.state.data.Cop +
                    ' ' +
                    this.state.data.Loc}
                </Text>
                {/* Extraction de l'heure du timestamp */}
                <Text
                  style={{
                    fontSize: 13,
                    alignItems: 'center',
                    textAlign: 'center',
                    marginBottom: 2
                  }}
                >
                  De{this.state.data.Date.substr(10, 6)} à
                  {this.state.data.DatMax.substr(10, 6)}
                </Text>
                <Text
                  style={{
                    fontSize: 13,
                    alignItems: 'center',
                    textAlign: 'center',
                    marginBottom: 2
                  }}
                >
                  Prix membre:{' '}
                  {this.state.data.prixmembre > 0
                    ? this.state.data.prixmembre
                    : 'gratuit'}
                </Text>
                {/* true && expression always evaluates to expression, and false && expression always evaluates to false.
                Therefore, if the condition is true, the element right after && will appear in the output. If it is false, React will ignore and skip it.*/}
                {this.state.data.NbMaxInvit > 0 && (
                  <Text
                    style={{
                      fontSize: 13,
                      alignItems: 'center',
                      textAlign: 'center',
                      marginBottom: 2
                    }}
                  >
                    Prix invité:{' '}
                    {this.state.data.prixinvite > 0
                      ? this.state.data.prixinvite
                      : 'gratuit'}
                  </Text>
                )}
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Button
                  icon={{
                    name: 'event-available',
                    type: 'MaterialIcons',
                    color: 'white',
                    size: 20
                  }}
                  title="Inscriptions"
                  borderRadius={3}
                  titleStyle={{ fontSize: 16 }}
                  buttonStyle={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: 135,
                    margin: 5
                  }}
                  containertyle={{
                    width: 50
                  }}
                  onPress={() =>
                    Alert.alert(
                      'En chantier',
                      "Cette fonctionnalité est en cours d'intégration. Revenez bientôt ! Vous pouvez vous inscrire via le site du B4C.",
                      [
                        {
                          text: "Je m'inscris via le site",
                          onPress: () =>
                            Linking.openURL(
                              'https://b4c.be/evenement/' +
                                this.props.navigation.state.params.idAct
                            )
                        },
                        { text: 'Je reviens plus tard' }
                      ],
                      { cancelable: true }
                    )
                  }
                />
                <Button
                  icon={{
                    name: 'navigation',
                    type: 'Feather',
                    color: 'white',
                    size: 20
                  }}
                  title={'Itinéraire'}
                  titleStyle={{ fontSize: 16 }}
                  borderRadius={3}
                  buttonStyle={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: 135,
                    margin: 5
                  }}
                  onPress={() =>
                    openMap({ query: this.state.data.adressMapQueryStr })
                  }
                />
              </View>
            </View>
            <View style={{ flex: 1 }}>
              <WebView
                useWebKit={true}
                originWhitelist={['*']}
                scrollEnabled={false}
                style={{
                  height: this.state.Height,
                  width: Dimensions.get('window').width - 40
                }}
                source={{
                  html: descriptionWebview + style + script,
                  baseUrl: ''
                }}
                javaScriptEnabled={true}
                onNavigationStateChange={event =>
                  this.onNavigationChange(event)
                }
              />
            </View>
          </ScrollView>
        );
      } else {
        return (
          <View
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          >
            <ActivityIndicator size="large" color="#9A8404" />
          </View>
        );
      }
      // Si pas loggué
    } else {
      return (
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
        >
          <Card title="Uniquement accessible aux membres">
          <Button
            icon={{
              name: 'login',
              type: 'material-community',
              color: 'white',
              marginRight: 5,
              size: 20
            }}
            featuredSubtitle="test"
            titleStyle={{fontSize: 16}}
            title="Identifiez-vous ou devenez membre"
            borderRadius={3}
            onPress={() => this.props.navigation.navigate('Home')}
          />
          </Card>
          
        </View>
      );
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActDetail);
