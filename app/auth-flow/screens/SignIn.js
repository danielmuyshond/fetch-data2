import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  Alert,
  Keyboard,
  Linking,
  TouchableWithoutFeedback,
  Image
} from 'react-native';
import { removeUserToken } from '../../actions';
import { Button, Input } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { saveUserToken } from '../../actions';
import styles from '../../styles';

const navigateAction = NavigationActions.navigate({
  routeName: 'Home',
  params: {}
});

const mapStateToProps = state => ({
  token: state.token
});

const mapDispatchToProps = dispatch => ({
  saveUserToken: () => dispatch(saveUserToken())
});

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pp: '',
      p: ''
    };
  }
  // const { pp, p } = this.props;

  // IDPPH est le ID de l'user dans le DB
  signInAsync = idpph => {
    this.props
      .saveUserToken(idpph)
      .then(() => {
        this.props.navigation.dispatch(navigateAction);
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  handleLogin = () => {
    console.log('pressed');
    Keyboard.dismiss();
    if (!this.state.pp) {
      Alert.alert(
        "Erreur d'identification",
        'Veuillez entrer un N° Premium pass, s.v.p.'
      );
    } else if (!this.state.p) {
      Alert.alert(
        "Erreur d'identification",
        'Veuillez entrer votre mot de passe, s.v.p.'
      );
    } else if (this.state.p && this.state.pp) {
      // console.log(`Button login pressed. Login :${this.state.pp} ${this.state.p}`);
      this.apiLoginFetch();
    }
  };

  async apiLoginFetch() {
    try {
      const response = await fetch(
        `https://b4c.be/b4c_app_API/applogin.php?pp=${this.state.pp}&p=${
          this.state.p
        }`
      );
      const responseJson = await response.json();
      // console.log(responseJson);
      if (responseJson.pp == false) {
        Alert.alert("Erreur d'identification", 'Indentifiant incorrect');
      } else if (responseJson.p == false) {
        Alert.alert("Erreur d'identification", 'Mot de passe incorrect');
      } else if (responseJson.pp == true && responseJson.p == true) {
        this.signInAsync(responseJson.IDPPH);
        Alert.alert('Identifiants corrects', 'Bienvenue ' + responseJson.Pre);
      }
    } catch (error) {
      // console.error(error);
    }
  }
  
  render() {
    return (
      <KeyboardAvoidingView style={styles.homeCard} behavior="padding">
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View>
            <Image
              style={styles.homelogo}
              source={require('../../../assets/b4c-app_nopic.png')}
            />
            <Input
              label="Identifiant"
              placeholder="mail ou premium pass"
              selectionColor="#cdb640"
              containerStyle={styles.loginInput}
              onChangeText={pp => this.setState({ pp: pp.toLowerCase() })}
              value={this.state.pp}
              underlineColorAndroid="transparent"
              maxLength={254}
              autoFocus
              onSubmitEditing={Keyboard.dismiss}
            />
            <Input
              label="Mot de passe"
              secureTextEntry
              placeholder="Mot de passe"
              selectionColor="#cdb640"
              containerStyle={styles.loginInput}
              onChangeText={p => this.setState({ p: p })}
              value={this.state.p}
              underlineColorAndroid="transparent"
              maxLength={256}
              onSubmitEditing={Keyboard.dismiss}
            />
            <Button
              icon={{
                name: 'login',
                type: 'material-community',
                color: 'white',
                marginRight: 5
              }}
              title="Connection"
              selectionColor="#cdb640"
              backgroundColor="#998702"
              borderRadius={3}
              containerStyle={styles.loginInput}
              onPress={this.handleLogin}
            />
            <Button
              icon={{
                name: 'wallet-membership',
                type: 'material-community',
                color: 'white',
                marginRight: 5,
                size: 20
              }}
              title="Devenir membre"
              borderRadius={3}
              containerStyle={styles.loginInput}
              onPress={() => {
                Alert.alert(
                  'Devenir membre',
                  'Vous allez être redirigé vers un formulaire à compléter via le site du B4C.',
                  [
                    {
                      text: 'OK, super. Allons-y !',
                      onPress: () =>
                        Linking.openURL('https://b4c.be/devenir-membre/')
                    },
                    { text: 'Pas maintenant, merci.' }
                  ],
                  { cancelable: true }
                );
              }}
            />
            <View style={{ height: 140 }} />
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);
