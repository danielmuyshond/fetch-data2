import React, { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Dimensions } from 'react-native';

const listItemWidth = (Dimensions.get('window').width / 3) * 2.8;
const listItemImgSize = listItemWidth / 3;
const topMenuButtonSize = listItemWidth / 2.2;

const sFlatlistItem = StyleSheet.create({
  flatlistbg: {
    backgroundColor: '#fff'
  },
  listItemContainer: {
    width: listItemWidth,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  itemTitleContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    flexWrap: 'wrap',
    paddingHorizontal: 5
  },
  itemTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    flexWrap: 'wrap',
    color: '#42413e',
    textAlign: 'center',
  },
  itemDate: {
    fontSize: 16,
    fontWeight: 'bold',
    flexWrap: 'wrap',
    color: '#42413e',
    alignSelf: 'center',
  },
  mbrNomContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    flexWrap: 'wrap',
    paddingHorizontal: 5
  },
  mbrListNom: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#42413e'
  },
  mbrListPre: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#d6d698'
  }
});

export default sFlatlistItem;
