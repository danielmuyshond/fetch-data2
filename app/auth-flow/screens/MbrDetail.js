
import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { Text, Image } from 'react-native-elements';
import styles from './styles';
// import { SocialIcon } from 'react-native-elements';

export default class MbrDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      data: []
    };
  }

  componentDidMount() {
    this.apiActFetch();
  }

  // Fetch des data sur le SQL Server B4C via l'API codée en PHP, hébergée sur le FTP du site B4C
  async apiActFetch() {
    try {
      let response = await fetch(
        "https://b4c.be/b4c_app_API/get_profile_info.php?idpph=" +
          this.props.navigation.state.params.IDPPH
      );
      let responseJson = await response.json();
      this.setState({
        data: responseJson,
        isLoading: false
      });
      // console.log(this.state.data);
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { isLoading, data } = this.state;
    const { navigation } = this.props;
    if (!isLoading) {
      return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center"}}>
          <View>
            {/* <Image
              source={{uri: 'https://b4c.be/wp-content/uploads/b4c/banners/banner-default.jpg'}}
              style={{ width: 200, height: 200 }}
              PlaceholderContent={<ActivityIndicator />}
            /> */}
            <Image
              // containerStyle={{width: 200, height: 200}}
              source={{ uri: navigation.state.params.Photo_Chemin}}
              style={{ width: 200, height: 200, borderRadius: 200/2  }}
              containerStyle={{ marginBottom: 30}}
            />
            <Text h3>{navigation.state.params.FullName}</Text>
            <Text style={styles.title}>
              email: {data.email}
            </Text>
            <Text style={styles.title}>
              gsm: {data.gsm}
            </Text>

          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#9A8404" />
        </View>
      );
    }
  }
}
