import { StyleSheet } from "react-native";
// import { getStatusBarHeight } from "react-native-status-bar-height";
import { Dimensions } from "react-native";

const deviceWidth = Dimensions.get("window").width;
const topButtonAgendaLargeur = (deviceWidth / 2) - 2;
// const logosize = (deviceWidth / 3) * 2;

export default StyleSheet.create({
  container: {
    flex: 1,
    width: deviceWidth,
    // flexDirection: "column",
    backgroundColor: "#f7f6f2",
    // alignItems: "center",
    // justifyContent: "center"
  },
  topButtonAgenda: {
    width: topButtonAgendaLargeur,
  },
  fullDeviceWidth: {
    width: deviceWidth,
  }
  
});
