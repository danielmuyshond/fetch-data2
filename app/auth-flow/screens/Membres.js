import React, { Component } from 'react';

import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { SearchBar, Card, Image, Button } from 'react-native-elements';
import sFlatlistItem from './Membres-styles';
import styles from './styles';

const mapStateToProps = state => ({
  token: state.token
});

class Membres extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      loadingMore: false,
      data: [],
      error: null
    };
    this.arrayholder = [];
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    const url = `https://b4c.be/b4c_app_API/get_PPH.php?p=1`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res.membres,
          error: res.error || null,
          loading: false
        });
        // console.log(res.membres);
        this.arrayholder = res.membres;
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  searchFilterFunction = text => {
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.Nom.toUpperCase()}   
    ${item.Pre.toUpperCase()} ${item.Libelle.toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ data: newData });
  };

  _renderItem = ({ item }) => (
    <Card>
      <View style={sFlatlistItem.listItemContainer}>
        <Image
          style={sFlatlistItem.itemImage}
          source={{ uri: item.Photo_Chemin }}
        />
        <View style={sFlatlistItem.mbrNomContainer}>
          <Text style={sFlatlistItem.mbrListPre}>
            {item.Pre.toUpperCase().replace(/\/\'/, "'") + ' '}
          </Text>
          <Text style={sFlatlistItem.mbrListNom}>
            {item.Nom.replace(/\/\'/, "'")}
          </Text>
          <Text style={sFlatlistItem.mbrListLibel}>
            {item.Nom_Comm.trim()
              ? item.Nom_Comm.replace(/&amp;amp;|&amp;/gi, '&')
              : item.Libelle.replace(/&amp;/gi, '&')}
          </Text>
        </View>
      </View>
      <Button
        icon={{ name: 'face', type: 'MaterialIcons', color: 'white', size: 20 }}
        title="Voir le profil"
        borderRadius={3}
        backgroundColor="#998702"
        containerStyle={{
          borderRadius: 3,
          marginTop: 10,
          width: 150,
          alignSelf: 'flex-end'
        }}
        titleStyle={{ fontSize: 16 }}
        onPress={() =>
          this.props.navigation.navigate('MbrDetail', {
            IDPPH: item.IDPPH,
            Photo_Chemin: item.Photo_Chemin,
            FullName: item.FullName
          })
        }
      />
    </Card>
  );

  // renderFooter () {
  //   return this.state.loading ? <View><Text>Loading...</Text></View> : null
  // }

  render() {
    if (this.props.token.token !== null) {
      if (this.state.loading) {
        return (
          <View style={styles.container}>
            <ActivityIndicator size="large" color="#9A8404" />
          </View>
        );
      } else {
        return (
          <View>
            <View
              style={[
                {
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  position: 'absolute',
                  top: 0,
                  zIndex: 1
                },
                styles.fullDeviceWidth
              ]}
            >
              <SearchBar
                containerStyle={{ flex: 1 }}
                placeholder="Recherche par nom ou entreprise ..."
                lightTheme
                round
                onChangeText={text => this.searchFilterFunction(text)}
                autoCorrect={false}
              />
            </View>
            <FlatList
              data={this.state.data}
              renderItem={this._renderItem}
              keyExtractor={item => item.IDPPH}
              // renderFooter={this.renderFooter.bind}
              // ItemSeparatorComponent={this.renderSeparator}
              bounces={false}
              windowSize={11}
              maxToRenderPerBatch={1}
              updateCellsBatchingPeriod={30}
            />
          </View>
        );
      }
    } else {
      return (
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
        >
          <Card title="Uniquement accessible aux membres">
            <Button
              icon={{
                name: 'login',
                type: 'material-community',
                color: 'white',
                marginRight: 5,
                size: 20
              }}
              featuredSubtitle="test"
              titleStyle={{ fontSize: 16 }}
              title="Identifiez-vous ou devenez membre"
              borderRadius={3}
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </Card>
        </View>
      );
    }
  }
}

export default connect(mapStateToProps)(Membres);
