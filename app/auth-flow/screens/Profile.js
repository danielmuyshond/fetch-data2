import React from "react";
import { View, Text, Linking, Alert } from "react-native";
import { Card, Button, Tile } from "react-native-elements";
import styles from '../../styles';
import { isSignedIn, setStorage } from "../auth";

class Membres extends React.Component {
  constructor(props) {
    super(props);
    this.state = { signedIn: false };
    isSignedIn()
      .then(response => this.setState({ signedIn: response }))
      .catch(error => alert("Oops! Something broked"));
  }

  static navigationOptions = {
    title: 'Membres',
  };

  render() {
    const { navigate } = this.props.navigation;
    if (!this.state.signedIn) {
      return (
        <View style={styles.container}>

          <Card containerStyle={styles.homeCard}>
          <Tile
            icon={{
              name: "lock",
              type: "font-awesome",
              size: 82,
              color: "#675600"
            }} // optional
            title="Cette section est réservée aux membres B4C"
            imageContainerStyle={styles.homelogo}
            containerStyle={{alignSelf: "center"}}
            iconContainerStyle={{alignSelf: "center"}}
            // contentContainerStyle={{ height: 70 }}
            titleStyle={{ textAlign: "center", color: "#998702",backgroundColor: "#f5f5f6" }}
          />
              <Button
                icon={{ name: "wallet-membership", type: "material-community" }}
                title="Devenir membre"
                borderRadius={3}
                backgroundColor="#26a85c"
                containerViewStyle={{
                  borderRadius: 3,
                  marginTop: 10,
                  width: 200,
                  alignSelf: "center"
                }}
                onPress={() => {
                  Alert.alert(
                    "Devenir membre",
                    "Vous allez être redirigé vers le formulaire à compléter via le site du B4C",
                    [
                      {
                        text: "OK, allons-y",
                        onPress: () =>
                          Linking.openURL("https://b4c.be/devenir-membre/")
                      },
                      { text: "Pas maintenant" }
                    ],
                    { cancelable: true }
                  );
                }}
              />
              <Button
                icon={{ name: "login", type: "material-community" }}
                title="S'identifier"
                backgroundColor="#998702"
                borderRadius={3}
                containerViewStyle={{
                  borderRadius: 3,
                  marginTop: 10,
                  width: 200,
                  alignSelf: "center"
                }}
                onPress={() => navigate("SignIn")}
              />
          </Card>
        </View>
        );
    } else {
      return (
        <View style={{ paddingVertical: 20 }}>
          <Card title="John Doe">
            <View
              style={{
                backgroundColor: "#bcbec1",
                alignItems: "center",
                justifyContent: "center",
                width: 80,
                height: 80,
                borderRadius: 40,
                alignSelf: "center",
                marginBottom: 20
              }}
            >
              <Text style={{ color: "white", fontSize: 28 }}>JD</Text>
            </View>
            <Button
              backgroundColor="#03A9F4"
              title="SIGN OUT"
              onPress={() => screenProps.signOut()}
            />
          </Card>
        </View>
      );
    }
  }
}

export default Membres;
