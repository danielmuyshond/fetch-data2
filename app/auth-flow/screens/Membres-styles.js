import { StyleSheet, Dimensions } from 'react-native';

const listItemWidth = (Dimensions.get('window').width / 1.5);
const listItemImgSize = listItemWidth / 3;
// const topMenuButtonSize = listItemWidth / 2;

const sFlatlistItem = StyleSheet.create({
  topMenuContainer: {
    paddingTop: 10,
    paddingBottom: 5,
    width: listItemWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bg: {
    backgroundColor: '#303837',
  },
  listItemContainer: {
    width: listItemWidth,
    backgroundColor: '#fff',
    color: '#fff',
    fontSize: 12,
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 7,
    margin: 5,
  },
  itemTitleContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    flexWrap: 'wrap',
    paddingHorizontal: 5,
  },
  itemImage: {
    backgroundColor: 'transparent',
    borderRadius: listItemImgSize / 2,
    marginHorizontal: 5,
    marginTop: 10,
    marginBottom: 5,
    height: listItemImgSize,
    width: listItemImgSize,
  },
  mbrNomContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    flexWrap: 'wrap',
    paddingHorizontal: 0,
    marginLeft: 5,
  },
  mbrListNom: {
    width: 200,
    fontWeight: 'bold',
    fontSize: 18,
    color: '#303837',
  },
  mbrListLibel: {
    width: 200,
    fontWeight: 'bold',
    fontSize: 14,
    color: '#998702',
  },
  mbrListPre: {
    width: 200,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#998702',
  },
});

export default sFlatlistItem;
