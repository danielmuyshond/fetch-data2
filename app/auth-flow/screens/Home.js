/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent */
/* eslint-disable global-require */
import React, { Component } from 'react';
// import { AppLoading, Asset } from 'expo';
import {
  View,
  Image,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
  Alert
} from 'react-native';
// import { NavigationActions } from 'react-navigation';
import { StackActions, NavigationActions } from 'react-navigation';
import { Button, Card, Input } from 'react-native-elements';
import { connect } from 'react-redux';
// import SignIn from './SignIn';
import styles from '../../styles';

// const mapDispatchToProps = dispatch => ({
//   getUserToken: () => dispatch(getUserToken()),
// });

// redux connect
import { removeUserToken, getUserToken, saveUserToken } from '../../actions';

const mapStateToProps = state => ({
  token: state.token
});

const mapDispatchToProps = dispatch => ({
  getUserToken: () => dispatch(getUserToken()),
  removeUserToken: () => dispatch(removeUserToken()),
  saveUserToken: () => dispatch(saveUserToken())
});

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Home' })]
});

class Home extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      pp: '',
      p: ''
    };
  }
  static navigationOptions = {
    headerLeft: null,
    isLoadingComplete: false,
    gesturesEnabled: false
  };

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = () => {
    this.props
      .getUserToken()
      .then(() => {
        this.props.token.token !== null
          ? this.setState({ isLogged: true })
          : this.setState({ isLogged: false });
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  componentDidMount() {
    this._bootstrapAsync();
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _signOutAsync = () => {
    // console.log('sign out button pressed');
    this.props.navigation.dispatch(resetAction);
    this.props
      .removeUserToken()
      .then(() => {
        // this.props.navigation.navigate('Welcome');
        if (this._isMounted) {
          this.setState({ isLogged: false });
        }
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  // IDPPH est le ID de l'user dans le DB
  signInAsync = idpph => {
    this.props
      .saveUserToken(idpph)
      .then(() => {
        this.setState({ isLogged: true });
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  async apiLoginFetch() {
    try {
      const response = await fetch(
        `https://b4c.be/b4c_app_API/applogin.php?pp=${this.state.pp}&p=${
          this.state.p
        }`
      );
      const responseJson = await response.json();
      // console.log(responseJson);
      if (responseJson.pp == false) {
        Alert.alert("Erreur d'identification", 'Indentifiant incorrect');
      } else if (responseJson.p == false) {
        Alert.alert("Erreur d'identification", 'Mot de passe incorrect');
      } else if (responseJson.pp == true && responseJson.p == true) {
        this.signInAsync(responseJson.IDPPH);
        Alert.alert('Identifiants corrects', 'Bienvenue ' + responseJson.Pre);
      }
    } catch (error) {
      // console.error(error);
    }
  }

  handleLogin = () => {
    // console.log('pressed');
    Keyboard.dismiss();
    if (!this.state.pp) {
      Alert.alert(
        'Connection',
        'Veuillez entrer une adresse e-mail ou un N° Premium pass'
      );
    } else if (!this.state.p) {
      Alert.alert('Mot de passe', 'Veuillez entrer votre mot de passe, s.v.p.');
    } else if (this.state.p && this.state.pp) {
      // console.log(`Button login pressed. Login :${this.state.pp} ${this.state.p}`);
      this.apiLoginFetch();
    }
  };

  render() {
    if (this.state.isLogged) {
      return (
        <View style={styles.container}>
          <Card containerStyle={styles.homeCard}>
            <Image
              style={styles.homelogo}
              source={require('../../../assets/icon.png')}
            />
            <Button
              icon={{
                name: 'logout',
                type: 'material-community',
                color: 'white',
                size: 20
              }}
              title="Déconnexion"
              borderRadius={3}
              containerViewStyle={{ borderRadius: 3, marginTop: 10 }}
              buttonStyle={{ width: 180 }}
              onPress={this._signOutAsync}
            />
          </Card>
        </View>
      );
    } else {
      return (
        <KeyboardAvoidingView style={styles.homeCard} behavior="padding">
          <TouchableWithoutFeedback
            onPress={Keyboard.dismiss}
            accessible={false}
          >
            <View>
              <Image
                style={styles.homelogo}
                source={require('../../../assets/b4c-app_nopic.png')}
              />
              <Input
                label="Identifiant"
                placeholder="mail ou premium pass"
                selectionColor="#cdb640"
                containerStyle={styles.loginInput}
                onChangeText={pp => this.setState({ pp: pp.toLowerCase() })}
                value={this.state.pp}
                underlineColorAndroid="transparent"
                maxLength={254}
                autoFocus={false}
                onSubmitEditing={Keyboard.dismiss}
              />
              <Input
                label="Mot de passe"
                secureTextEntry
                placeholder="Mot de passe"
                selectionColor="#cdb640"
                containerStyle={styles.loginInput}
                onChangeText={p => this.setState({ p: p })}
                value={this.state.p}
                underlineColorAndroid="transparent"
                maxLength={256}
                onSubmitEditing={Keyboard.dismiss}
              />
              <Button
                icon={{
                  name: 'login',
                  type: 'material-community',
                  color: 'white',
                  marginRight: 5
                }}
                title="Connection"
                borderRadius={3}
                containerStyle={styles.loginInput}
                onPress={this.handleLogin}
              />
              <Button
                icon={{
                  name: 'wallet-membership',
                  type: 'material-community',
                  color: 'white',
                  marginRight: 5,
                  size: 20
                }}
                title="Devenir membre"
                borderRadius={3}
                buttonStyle={{ backgroundColor: '#26a85c' }}
                containerStyle={styles.loginInput}
                onPress={() => {
                  Alert.alert(
                    'Devenir membre',
                    'Vous allez être redirigé vers un formulaire à compléter via le site du B4C.',
                    [
                      {
                        text: 'OK, super. Allons-y !',
                        onPress: () =>
                          Linking.openURL('https://b4c.be/devenir-membre/')
                      },
                      { text: 'Pas maintenant, merci.' }
                    ],
                    { cancelable: true }
                  );
                }}
              />
              <View style={{ height: 140 }} />
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      );
    }
  }
  // return (
  //   <View style={styles.container}>
  //     <Card containerStyle={styles.homeCard}>
  //       <Image
  //         style={styles.homelogo}
  //         source={require("../../../assets/icon.png")}
  //       />

  //       <Button
  //         icon={{
  //           name: "logout",
  //           type: "material-community",
  //           color: "white",
  //           size: 20
  //         }}
  //         title="Déconnexion"
  //         borderRadius={3}
  //         containerViewStyle={{ borderRadius: 3, marginTop: 10 }}
  //         buttonStyle={{ width: 180 }}
  //         onPress={this._signOutAsync}
  //       />
  //     </Card>
  //   </View>
  // );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
