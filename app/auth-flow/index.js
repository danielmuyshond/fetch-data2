/* eslint-disable global-require */
import React from 'react';
import { View, Image } from 'react-native';
import {
	createStackNavigator,
	createBottomTabNavigator,
	createSwitchNavigator,
	createAppContainer,
} from 'react-navigation';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { FontAwesome } from '@expo/vector-icons';

import AuthLoadingScreen from './screens/AuthLoadingScreen';
import SignIn from './screens/SignIn';
import Home from './screens/Home';
import Welcome from './screens/Welcome';
import Activites from './screens/Activites';
import ActDetail from './screens/ActDetail';
import Membres from './screens/Membres';
import MbrDetail from './screens/MbrDetail';
import Profile from './screens/Profile';
// import SignUp from "./screens/SignUp";

const LogoTitle = () => (
	<Image
		source={require('../../assets/header.jpg')}
		style={{ width: 200, height: 45, marginBottom: 5 }}
	/>
);

const HomeStack = createStackNavigator(
	{
		Home: {
			screen: Home,
		},
	},
	{
		initialRouteName: 'Home',
		defaultNavigationOptions: {
			headerTitle: <LogoTitle />,
			headerStyle: {
				backgroundColor: '#fff',
				borderBottomWidth: 0,
			},
			headerTintColor: '#353638',
			headerTitleStyle: {
				fontWeight: 'bold',
				color: '#353640',
				zIndex: 1,
				fontSize: 14,
				lineHeight: 23,
			},
		},
	},
);

const ActivitesStack = createStackNavigator(
	{
		Activites: {
			screen: Activites,
		},
		ActDetail: {
			screen: ActDetail,
		},
	},
	{
		initialRouteName: 'Activites',
		defaultNavigationOptions: {
			headerTitle: <LogoTitle />,
			headerStyle: {
				backgroundColor: '#fff',
				borderBottomWidth: 0,
			},
			headerTintColor: '#353638',
			headerTitleStyle: {
				fontWeight: 'bold',
				color: '#353640',
				zIndex: 1,
				fontSize: 14,
				lineHeight: 23,
			},
		},
	},
);

const MembresStack = createStackNavigator(
	{
		Membres: {
			screen: Membres,
		},
		MbrDetail: {
			screen: MbrDetail,
		},
	},
	{
		initialRouteName: 'Membres',
		defaultNavigationOptions: {
			headerTitle: <LogoTitle />,
			headerStyle: {
				backgroundColor: '#fff',
				borderBottomWidth: 0,
			},
			headerTintColor: '#353638',
			headerTitleStyle: {
				fontWeight: 'bold',
				color: '#353640',
				zIndex: 1,
				fontSize: 14,
				lineHeight: 23,
			},
		},
	},
);

const ProfileStack = createStackNavigator(
	{
		Profile: {
			screen: Profile,
		},
	},
	{
		initialRouteName: 'Profile',
		defaultNavigationOptions: {
			headerTitle: <LogoTitle />,
			headerStyle: {
				backgroundColor: '#fff',
				borderBottomWidth: 0,
			},
			headerTintColor: '#353638',
			headerTitleStyle: {
				fontWeight: 'bold',
				color: '#353640',
				zIndex: 1,
				fontSize: 14,
				lineHeight: 23,
			},
		},
	},
);


const TabNavigator = createBottomTabNavigator(
	{
		Home: {
			screen: HomeStack,
			navigationOptions: {
				tabBarLabel: 'Accueil',
				tabBarIcon: ({ tintColor }) => (
					<FontAwesome name="home" size={24} color={tintColor} />
				),
			},
		},
		Activites: {
			screen: ActivitesStack,
			navigationOptions: {
				tabBarLabel: 'Activités',
				tabBarIcon: ({ tintColor }) => (
					<FontAwesome name="calendar" size={24} color={tintColor} />
				),
			},
		},
		Membres: {
			screen: MembresStack,
			navigationOptions: {
				tabBarLabel: 'Membres',
				tabBarIcon: ({ tintColor }) => (
					<FontAwesome name="users" size={24} color={tintColor} />
				),
			},
		},
	},
	{
		initialRouteName: 'Home',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: '#071112',
				paddingTop: getStatusBarHeight(),
				borderBottomWidth: 0,
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				fontWeight: 'bold',
				color: 'black',
				zIndex: 0,
				fontSize: 14,
				lineHeight: 23,
			},
		},
		tabBarOptions: {
			activeBackgroundColor: '#596261',
			inactiveBackgroundColor: '#596261',
			activeTintColor: '#fff',
			inactiveTintColor: '#838f8e',
			showIcon: true,
			hideShadow: true,
			style: {
				backgroundColor: '#303837',
			},
		},
	},
);

const AuthStack = createStackNavigator(
	{
		Welcome: {
			screen: Welcome,
		},
		SignIn: { screen: SignIn },
	},
	{
		initialRouteName: 'Welcome',
		defaultNavigationOptions: {
			headerTitle: <Image
				source={require('../../assets/header.jpg')}
				style={{ width: 200, height: 45, marginBottom: 5 }}
			/>,
			headerStyle: {
				backgroundColor: '#fff',
				borderBottomWidth: 0,
			},
			headerTintColor: '#353638',
			headerTitleStyle: {
				fontWeight: 'bold',
				color: '#353640',
				zIndex: 1,
				fontSize: 14,
				lineHeight: 23,
			},
		},
	},
);


const MainStack = createSwitchNavigator(
	{
		AuthLoading: AuthLoadingScreen,
		Auth: { screen: AuthStack },
		Tab: { screen: TabNavigator },
	},
	{
		initialRouteName: 'Tab',
		defaultNavigationOptions: {
			headerTitle: <LogoTitle />,
			headerStyle: {
				backgroundColor: '#071112',
				paddingTop: getStatusBarHeight(),
				borderBottomWidth: 0,
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				fontWeight: 'bold',
				color: 'black',
				zIndex: 0,
				fontSize: 14,
				lineHeight: 23,
			},
		},
	},
);

const Hello = createAppContainer(MainStack);
export default Hello;
