import { StyleSheet, Dimensions } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

const deviceWidth = Dimensions.get('window').width;
const lgbuttonwidth = deviceWidth / 1.2;
const logosize = (deviceWidth / 3) * 2;

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#303837',
		alignItems: 'center',
		justifyContent: 'center',
		paddingTop: getStatusBarHeight(),
	},
	homelogo: {
		width: logosize / 2,
		height: logosize / 2,
		borderRadius: logosize / 2 / 2,
		padding: 10,
		marginVertical: 20,
		alignSelf: 'center',
	},
	title: {
		fontWeight: 'bold',
		fontSize: 24,
	},
	center: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	defaultText: {
		fontWeight: 'bold',
		fontSize: 12,
	},
	button: {
		width: lgbuttonwidth,
		height: 60,
		marginVertical: 5,
	},
	mdbutton: {
		borderRadius: 3,
		marginTop: 10,
		width: 200,
	},
	lgbutton: {
		borderRadius: 3,
		marginTop: 10,
		width: lgbuttonwidth,
		alignSelf: 'center',
	},
	homeCard: {
		flex: 1,
		paddingVertical: 0,
		margin: 0,
		width: deviceWidth,
		backgroundColor: '#f5f5f6',
		alignItems: 'center',
		justifyContent: 'center',
	},
	buttonBorders: {
		borderColor: '#fff',
		borderWidth: 1,
		width: lgbuttonwidth,
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 5,
	},
	buttonText: {
		textTransform: 'uppercase',
		color: '#fff',
		fontSize: 12,
		fontWeight: 'bold',
		backgroundColor: 'transparent',
	},
	greenbutton: {
		color: '#59ff92',
		borderColor: '#59ff92',
	},
	loginInput: {
		width: lgbuttonwidth,
		margin: 5,
	},
});
