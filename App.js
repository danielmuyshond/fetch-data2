import React from 'react';
import { ThemeProvider } from 'react-native-elements';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import Hello from './app/auth-flow'; // import Hello from './src/navigation/Hello';
import store from './app/store';

const theme = {
	colors: {
		primary: '#a39000',
	}
}

const App = () => (
	<Provider store={store}>
		<StatusBar translucent={false} barStyle="dark-content" />
		<ThemeProvider theme={theme}>
			<Hello />
		</ThemeProvider>
	</Provider>
);

export default App;
